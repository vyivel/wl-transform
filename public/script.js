document.addEventListener("DOMContentLoaded", function () {
  // Transform utils

  const fancyNames = {
    0: "WL_OUTPUT_TRANSFORM_NORMAL",
    90: "WL_OUTPUT_TRANSFORM_90",
    180: "WL_OUTPUT_TRANSFORM_180",
    270: "WL_OUTPUT_TRANSFORM_270",
    "f-0": "WL_OUTPUT_TRANSFORM_FLIPPED",
    "f-90": "WL_OUTPUT_TRANSFORM_FLIPPED_90",
    "f-180": "WL_OUTPUT_TRANSFORM_FLIPPED_180",
    "f-270": "WL_OUTPUT_TRANSFORM_FLIPPED_270",
  };

  const orderedNames = [
    "0",
    "90",
    "180",
    "270",
    "f-0",
    "f-90",
    "f-180",
    "f-270",
  ];

  // Taken from wlroots
  function invertTransform(name) {
    let tr = orderedNames.indexOf(name);
    if (tr & 1 && !(tr & 4)) {
      tr ^= 2;
    }
    return orderedNames[tr];
  }

  // Taken from wlroots
  function composeTransform(name_a, name_b) {
    const tr_a = orderedNames.indexOf(name_a);
    const tr_b = orderedNames.indexOf(name_b);
    const flipped = (tr_a ^ tr_b) & 4;
    const rotation_mask = 1 | 2;
    let rotated;
    if (tr_b & 4) {
      rotated = (tr_b - tr_a) & rotation_mask;
    } else {
      rotated = (tr_a + tr_b) & rotation_mask;
    }
    return orderedNames[flipped | rotated];
  }

  // JS utils

  function addChangeListener(el, cb) {
    el.addEventListener("change", cb);
    if (el.checked) {
      cb();
    }
  }

  // Main logic

  const container_el = document.getElementById("container");

  const output_el = document.getElementById("output");
  const buffer_el = document.getElementById("buffer");

  const lock_el = document.getElementById("lock-input");

  const result_name_el = document.getElementById("result-transform-name");

  let output_locked = false;

  function updateTransform(target_el, name_el, value, inverted) {
    if (inverted) {
      value = invertTransform(value);
    }

    target_el.dataset.transform = value;
    name_el.innerHTML = fancyNames[value];

    updateAll();
  }

  function registerTarget(target_el, controls_id, name_id, inverted) {
    const controls_el = document.getElementById(controls_id);
    const name_el = document.getElementById(name_id);

    const inputs = Array.from(controls_el.getElementsByTagName("input"));
    inputs.forEach((input_el) => {
      addChangeListener(input_el, () =>
        updateTransform(target_el, name_el, input_el.value, inverted)
      );
    });
  }

  // TODO hmmmmm
  registerTarget(output_el, "output-controls", "output-transform-name", false);
  registerTarget(buffer_el, "buffer-controls", "buffer-transform-name", true);

  addChangeListener(lock_el, () => {
    output_locked = lock_el.checked;
    updateAll();
  });

  function updateAll() {
    if (output_locked) {
      container_el.dataset.transform = invertTransform(
        output_el.dataset.transform
      );
    } else {
      container_el.dataset.transform = "0";
    }

    result_name_el.innerHTML =
      fancyNames[
        composeTransform(
          output_el.dataset.transform,
          buffer_el.dataset.transform
        )
      ];
  }
});
